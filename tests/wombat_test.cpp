#include "wombat/wombat.hpp"
#include "catch2/catch.hpp"

TEST_CASE( "add_one", "[adder]" ) {
  REQUIRE( wbt::add_one(0) == 1 );
  REQUIRE( wbt::add_one(123) == 124 );
  REQUIRE( wbt::add_one(-1) == 0 );
}
