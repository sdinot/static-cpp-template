#include "wombat/wombat.hpp"
#include <iostream>

int main(int argc, char **argv) {
  int result = wbt::add_one(1);
  std::cout << "1 + 1 = " << result << '\n';
}
