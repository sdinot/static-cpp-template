#include "wombat/wombat.hpp"

namespace wbt {

int add_one(int x) {
  return x + 1;
}

} // namespace wbt
