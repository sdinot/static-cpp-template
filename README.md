# Welcome to Wombat

![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)

![C++ logo](resources/cpp_logo.png)

# Prerequisites

Building Wombat requires the following software installed:

* A C++20-compliant compiler
* CMake `>= 3.9`
* Doxygen (optional, documentation building is skipped if missing)
* The testing framework [Catch2](https://github.com/catchorg/Catch2) for
  building the test suite

# Building Wombat

The following sequence of commands builds Wombat. It assumes that your current
working directory is the top-level directory of the freshly cloned repository:

```bash
$ cmake -S . -B build -DCMAKE_BUILD_TYPE=RelWithDebInfo
$ cmake --build build
```

The build process can be customized with the following CMake variables, which
can be set by adding `-D<var>={ON, OFF}` to the `cmake` call:

* `BUILD_TESTING`: Enable building of the test suite (default: `ON`)
* `BUILD_DOCS`: Enable building the documentation (default: `ON`)

# Testing Wombat

When built according to the above explanation (with `-DBUILD_TESTING=ON`), the
C++ test suite of Wombat can be run from the top-level directory:

```bash
$ ./build/tests/tests
```

# Documentation

Wombat provides a Doxygen documentation. You can build the documentation locally
by making sure that Doxygen is installed on your system and running this command
from the top-level build directory:

```bash
$ cmake --build build --target doxygen
```

The web documentation can then be browsed by opening `build/doc/html/index.html`
in your browser.

# Copyright and license

Copyright (c) 2023, John Doe, https://example.com

Wombat is licensed under the terms of the MIT license. See [LICENSE](LICENSE)
file for more information.
